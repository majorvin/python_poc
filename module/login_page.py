from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from module.base_page import BasePage

class LoginPage(BasePage):
  email = (By.NAME, 'Email')
  nextButton = (By.NAME, 'signIn')
  passwd = (By.NAME, 'Passwd')
  sendButton = (By.XPATH, '//div[@class="form-panel second"]//input[@name="signIn"]')

  def set_email(self, email):
    emailElem = self.driver.find_element(*LoginPage.email)
    emailElem.send_keys(email)

  def set_password(self, passwrd):
    wait = WebDriverWait(self.driver, 10)
    passwdElem = wait.until(EC.element_to_be_clickable(LoginPage.passwd))
    passwdElem.send_keys(passwrd)

  def click_next_btn(self):
    element = self.driver.find_element(*LoginPage.nextButton)
    element.click()

  def click_submit_btn(self):
    element = self.driver.find_element(*LoginPage.sendButton)
    element.click()

  def login(self, email, passwrd):
    self.set_email(email)
    self.click_next_btn()
    self.set_password(passwrd)
    self.click_submit_btn()
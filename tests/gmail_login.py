import unittest
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class LoginTests(unittest.TestCase):
  def setUp(self):
    self.driver = webdriver.Remote(
       command_executor='http://127.0.0.1:4444/wd/hub',
       desired_capabilities=DesiredCapabilities.CHROME)
    self.driver.get("https://www.gmail.com")

  def tearDown(self):
    self.driver.close()

  def test_search(self):
    wait = WebDriverWait(self.driver, 10)


    email = self.driver.find_element_by_name("Email")
    email.send_keys("helloworld")
    submit = self.driver.find_element_by_name("signIn")
    submit.click()
    element = wait.until(EC.element_to_be_clickable((By.NAME,'Passwd')))
    passwd = self.driver.find_element_by_name("Passwd")
    passwd.send_keys("helloworld")
    submit = self.driver.find_element_by_xpath("//div[@class='form-panel second']//input[@name='signIn']")
    submit.click()

    assert "1" in "2"

if __name__ == "__main__":
  unittest.main()
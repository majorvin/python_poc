import unittest
import time
import pdb

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from module.login_page import LoginPage

class LoginTests(unittest.TestCase):
  def setUp(self):
    self.driver = webdriver.Remote(
       command_executor='http://127.0.0.1:4444/wd/hub',
       desired_capabilities=DesiredCapabilities.CHROME)
    self.driver.get("https://www.gmail.com")

  def tearDown(self):
    self.driver.close()

  def test_login_helloworld(self):
    login_page = LoginPage(self.driver)
    login_page.login("helloworld", "helloworld")

    assert "1" in "2"

  def test_login_earth(self):
    login_page = LoginPage(self.driver)
    login_page.login("helloearth", "earth")

    assert "1" in "2"

if __name__ == "__main__":
  unittest.main()